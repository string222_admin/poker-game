package com.string.pokergame.client;

import java.awt.image.BufferedImage;

/**
 * 封装按钮的属性和图片
 */
public class Button {
    private final int x;
    private final int y;
    private final int width;
    private final int height;
    BufferedImage image;

    public Button(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = GameClient.button1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
